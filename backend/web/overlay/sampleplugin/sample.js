/*
    With or without /sampleplugin/connect the overlay will have its osc settings available in the console

    Global vars:
    oscIP
    oscPort
    pluginSettings
*/

function getOSCMessage(message){
    if(message.address == "/sampleplugin/connect/success"){
        console.log("OSC CONNECTED!");
        document.querySelector("#oscInfo").innerHTML = "OSC Connected!<br>Here's the info:<br>IP: "+oscIP+"<br>Port: "+oscPort+"<br>Settings: "+JSON.stringify(pluginSettings);
    }
}

function onOSCOpen(){
    sendOSC("/sampleplugin/connect", 1.0);
}