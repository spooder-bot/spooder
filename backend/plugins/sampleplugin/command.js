class SamplePlugin {
	
	constructor() {
		this.onChat = this.onChat.bind(this);
	}
	
	/*List all your commands here to be accessible through the help command*/
	commandList = {
		
	};
	
	/*
		Put your command code here. Here's a quick reference:
		Chat text: message.message
		Sender's name: message.username
		Sender's emotes: message.tags.emotes
		
		There are a couple global functions to send OSC with:
		sendToTCP(address, content)
		sendToUDP(destination, address, content)
	*/
	onChat(message){
		
	}
}

module.exports = SamplePlugin;