var devMode = process.argv.length>2?process.argv[2] == "-d":false;
var initMode = process.argv.length>2?process.argv[2] == "-i":false;

var backendDir = __dirname+"/backend";

const fs = require('fs-extra');

try{
	var oauthFile = fs.readFileSync(backendDir+"/settings/oauth.json",{encoding:'utf8'});
	var oauth = JSON.parse(oauthFile);
	if(oauth['client-id'] == "editme" || oauth['client-secret' == "editme"]){
		console.error("No Twitch authentication credentials found. \n\
		Create an app on dev.twitch.tv and run 'npm run init' to fill in your client id and secret.");
	}
}catch(e){
	console.error(e);
	console.error("There's a problem with the oauth file. We'll keep running, but you won't be able to connect to chat.");
}

try{
	var configFile = fs.readFileSync(backendDir+"/settings/config.json",{encoding:'utf8'});
	var config = JSON.parse(configFile);
}catch(e){
	console.error(e);
	console.error("There's a problem with the config file. Switching to init mode.");
	initMode = true;
}

if(initMode){
	const readline = require("readline").createInterface({
		input: process.stdin,
		output: process.stdout
	});
	let artStr = String.raw`
	/ / _ \ \
	\_\(_)/_/
	 _//"\\_ 
	  /   \
	`;
	console.log("Hi there!\n"+artStr+"\n"+"Let's get your Spooder set up!\n");
	var initData = {};

	console.log("We'll need some app credentials for the Twitch API. \nYou'll need to set up your developer console at dev.twitch.tv to make app access credentials.\nOnce that's done, enter them here.");
	
	readline.question("Client ID: ", cid => {
		initData.clientId = cid;
		
		readline.question("Client Secret: ", cs => {
			initData.clientSecret = cs;
			
			readline.question("IP Address for OSC/Web hosting (Usually 192.168.*.*): ", ip => {
				initData.hostIP = ip;

				readline.question("What's your Spooder's name?: ", name => {
					
					initData.sName = name;
					
					readline.question(`What should the help command be? Could be !${initData.sName} or just !help: `, helpCmd => {
						if(helpCmd.startsWith("!")){
							initData.helpCmd = helpCmd.substring(1);
						}else{
							initData.helpCmd = helpCmd;
						}

						readline.question("What's your broadcaster username?: ", name => {
							initData.bName = name;
							
							
							readline.question("\nGreat! That's all the config essentials. You can change these and more in the Spooder Web UI. Close this process and call \n'npm run start' to start your Spooder (oOwOo)", name => {
								var newAuth = {
									"client-id":initData.clientId,
									"client-secret":initData.clientSecret
								};
								
								var newConfig = {
									"bot":{
										"sectionname":"Bot Settings",
										"bot_name":initData.sName,
										"help_command":initData.helpCmd,
										"introduction":"I'm a Spooder connected to the stream ^_^"
									},
									"broadcaster":{
										"sectionname":"Broadcaster",
										"username":initData.bName
									},"eventsub":{
										"sectionname":"Event Sub",
										"enabled":false,
										"callback_url":"",
									},"network":{
										"sectionname":"Network",
										"host":initData.hostIP,
										"host_port":"3000",
										"udp_clients":{},
										"osc_udp_port":9000,
										"osc_tcp_port":3333
									},"oscvars":{
										"sectionname":"OSC Variables"
									}
								};

								fs.writeFile(backendDir+"/settings/oauth.json", JSON.stringify(newAuth), "utf-8", (err, data)=>{
									fs.writeFile(backendDir+"/settings/config.json", JSON.stringify(newConfig), "utf-8", (err, data)=>{
										readline.close();
									});
								});
							});
						});
					});
				});
			});
		});
	});

	return;
}

const clientId = oauth['client-id'];
const clientSecret = oauth['client-secret'];

var uptime = 0;
//var upInterval = null;
var activeEvents = {};
var events = {};
var eventTypes = [];

var udpClients = {};

var activePlugins = {};

const {Chat, ChatEvents} = require("twitch-js");
const Axios = require("axios");
const OSC = require('osc-js');

const chmodr = require('chmodr');
const fsPromises = require('fs/promises');

const crypto = require("crypto");
const twitchSigningSecret = process.env.TWITCH_SIGNING_SECRET;

const verifyTwitchSignature = (req, res, buf, encoding) => {
  const messageId = req.header("Twitch-Eventsub-Message-Id");
  const timestamp = req.header("Twitch-Eventsub-Message-Timestamp");
  const messageSignature = req.header("Twitch-Eventsub-Message-Signature");
  const time = Math.floor(new Date().getTime() / 1000);
  console.log(`Message ${messageId} Signature: `, messageSignature);

  if (Math.abs(time - timestamp) > 600) {
    // needs to be < 10 minutes
    console.log(`Verification Failed: timestamp > 10 minutes. Message Id: ${messageId}.`);
    throw new Error("Ignore this request.");
  }

  if (!twitchSigningSecret) {
    console.log(`Twitch signing secret is empty.`);
    throw new Error("Twitch signing secret is empty.");
  }

  const computedSignature =
    "sha256=" +
    crypto
      .createHmac("sha256", twitchSigningSecret)
      .update(messageId + timestamp + buf)
      .digest("hex");
  console.log(`Message ${messageId} Computed Signature: `, computedSignature);

  if (messageSignature !== computedSignature) {
    throw new Error("Invalid signature.");
  } else {
    console.log("Verification successful");
  }
};

async function getPlugins(){
	try {
	  const dir = await fsPromises.opendir(backendDir+'/plugins');
	  activePlugins = {};
	  for await (const dirent of dir){
		activePlugins[dirent.name] = new (require(backendDir+'/plugins/'+dirent.name))();
	  }
	} catch (err) {
	  console.error(err);
	}
	
}
getPlugins();

try{
	commandFile = fs.readFileSync(backendDir+"/settings/commands.json", {encoding:'utf8'});
	events = JSON.parse(commandFile);
}catch(e){
	console.error(e);
	console.error("Something's wrong with the command file. Try saving commands in the web UI to fix it.")
	events = {};
}


for(let e in events){
	if(!eventTypes.includes(events[e].type)){
		eventTypes.push(events[e].type);
	}
}

udpClients = Object.assign(config.network.udp_clients);
var expressPort = devMode===false?config.network.host_port:3001;

const express = require('express');
const bodyParser = require("body-parser");
const extract = require('extract-zip');
const fileUpload = require('express-fileupload');
const router = express.Router();

const app = new express();

var token = "";
var refreshToken = "";

//For Event Subs
var appToken = "";

var scope = "chat:read chat:edit whispers:read whispers:edit";
var username = "";
var broadcasterUserID = 0;
channel = "#"+config.broadcaster.username;



var twitchParams = "?client_id="+clientId+
	"&client_secret="+clientSecret+
	"&grant_type=authorization_code"+
	"&code="+token+
	"&redirect_uri=http://localhost:"+expressPort+"/handle"+
	"&response_type=code";

app.use("/",router);
router.use("/overlay", express.static(backendDir+'/web/overlay'));
router.get("/overlay/get", async(req, res) => {
	pluginName = req.query.plugin;
	var pluginSettings = null;

	try{
		var thisPlugin = fs.readFileSync(backendDir+"/plugins/"+pluginName+"/settings.json", {encoding:'utf8'});
		pluginSettings = JSON.parse(thisPlugin);
	}catch(e){
		console.log("Plugin has no settings");
	}
	
	let oscInfo = {
		host: config.network.host,
		port: config.network.osc_tcp_port,
		settings: pluginSettings
	};

	res.send({express: JSON.stringify(oscInfo)});
});

if(devMode === false){
	router.use("/", express.static(__dirname+'/build'));
}

router.use(bodyParser.urlencoded({extended:true}));
router.use(bodyParser.json());
router.use("/install_plugin",fileUpload());
router.use(express.json({verify: verifyTwitchSignature}));

router.get('/handle', async (req,res)=>{

	console.log("Got code");
	token = req.query.code;
	twitchParams = "?client_id="+clientId+
		"&client_secret="+clientSecret+
		"&grant_type=authorization_code"+
		"&code="+token+
		"&redirect_uri=http://localhost:"+expressPort+"/handle"+
		"&response_type=code";
		
		
	await Axios.post('https://id.twitch.tv/oauth2/token'+twitchParams)
			.then((response)=>{
				
				if(typeof response.data.access_token != "undefined"){
					token = response.data.access_token;
					refreshToken = response.data.refresh_token;
				}
			}).catch(error=>{
				console.error(error);
				return;
			});
	console.log("Got token");
	
	await Axios({
		url: 'https://id.twitch.tv/oauth2/validate',
		method: 'get',
		headers:{
			"Authorization": "Bearer "+token
		}
	})
	.then((response)=>{
		
		username = response.data.login;
	}).catch(error=>{
		console.error(error);
		return;
	});

	res.redirect("http://localhost:"+expressPort+"/?user="+username);
	run();
});



router.get('/command_table', (req, res) => {
	let props = {
		"commands":events,
		"udpClients":udpClients
	};
	res.send({express: JSON.stringify(props)});
});

router.get('/server_config', (req, res) => {
	res.send({express: JSON.stringify(config)});
});

router.get('/udp_hosts', (req, res) => {
	res.send({express:JSON.stringify(udpClients)});
});

router.get('/server_state', async (req, res) => {

	var oscReturn = {
		host:config.network.host,
		port:config.network.osc_tcp_port
	}

	var hostReturn = {
		port:expressPort
	}
	
	if(token == ""){
		res.send({user:"", osc:oscReturn, host:hostReturn});
	}else{
		await Axios({
			url: 'https://id.twitch.tv/oauth2/validate',
			method: 'get',
			headers:{
				"Authorization": "Bearer "+token
			}
		})
		.then((response)=>{
			username = response.data.login;
			
			res.send({
				"user":username,
				"osc":oscReturn,
				"host":hostReturn
			});
		}).catch(error=>{
			console.error(error);
			return;
		});
	}
	
	
});

router.post("/saveCommandList", async (req, res) => {
	console.log("SAVING COMMAND LIST",req.body);
	
	fs.writeFile(backendDir+"/settings/commands.json", JSON.stringify(req.body), "utf-8", (err, data)=>{
		res.send({status:"SAVE SUCCESS"});
		console.log("I SAVED IT");
		events = req.body;
	});
});

router.post("/saveConfig", async (req, res) => {
	console.log("SAVING CONFIG",req.body);
	
	fs.writeFile(backendDir+"/settings/config.json", JSON.stringify(req.body), "utf-8", (err, data)=>{
		res.send({status:"SAVE SUCCESS"});
		console.log("I SAVED THE CONFIG");
		config = req.body;
	});
	
	restartOSC();
});

router.post('/install_plugin', async (req, res) => {
	console.log("INSTALL PLUGIN",req.files);
	
	try{
		if(!req.files){
			console.log("NO FILES FOUND");
			res.send({
				status: false,
				message: 'No file uploaded'
			})
		}else{
			let pluginZip = req.files.file;
			let pluginDirName = pluginZip.name.split(".")[0]
			if(!fs.existsSync(backendDir+"/tmp")){
				fs.mkdirSync(backendDir+"/tmp");
			}
			await pluginZip.mv(backendDir+"/tmp/"+pluginZip.name);
			console.log("EXTRACT ZIP");
			await extract(backendDir+"/tmp/"+pluginZip.name, {dir: backendDir+"/tmp/"+pluginDirName});
			
			console.log("MOVING FILES");
			await fs.move(backendDir+'/tmp/'+pluginDirName+"/command", backendDir+"/plugins/"+pluginDirName+"", {overwrite:true});
			await fs.move(backendDir+'/tmp/'+pluginDirName+"/overlay", backendDir+"/web/overlay/"+pluginDirName+"", {overwrite:true});
			await chmodr(backendDir+"/web/overlay/"+pluginDirName+"",0o777, (err) => {
				if(err) throw err;
				
			});
			await chmodr(backendDir+"/plugins/"+pluginDirName+"",0o777, (err) => {
				if(err) throw err;
				
			});
			console.log("COMPLETE!");
			fs.rm(backendDir+"/tmp/"+pluginZip.name);
			fs.rm(backendDir+"/tmp/"+pluginDirName, {recursive:true});
			getPlugins();
			res.send({
				status:true,
				message: "File Upload Success"
			});
		}
	}catch(e){
		console.error(e);
	}
});

router.post('/delete_plugin', async(req, res) => {
	
	let thisBody = req.body
	
	let pluginName = thisBody.pluginName;
	await fs.rm(backendDir+"/plugins/"+pluginName, {recursive:true});
	await fs.rm(backendDir+"/web/overlay/"+pluginName, {recursive:true});
	res.send(JSON.stringify({status:"SUCCESS"}));
	getPlugins();
});

router.post('/save_plugin', async(req, res) => {
	let newSettings = req.body;
	console.log("SAVING", backendDir+"/plugins/"+newSettings.pluginName+"/settings.json",newSettings);
	fs.writeFile(backendDir+"/plugins/"+newSettings.pluginName+"/settings.json", JSON.stringify(newSettings.settings), "utf-8", (err, data)=>{
		res.send({saveStatus:"SAVE SUCCESS"});
		console.log("I SAVED THE "+newSettings.pluginName+" SETTINGS!");
	});

	getPlugins();
});

router.get('/plugins', async (req, res) => {
	
	let pluginPacks = {};
	for(let a in activePlugins){
		let thisPluginPath = "http://"+config.network.host+":"+expressPort+"/overlay/"+a;
		let thisPlugin = JSON.parse(fs.readFileSync(backendDir+"/plugins/"+a+"/settings.json", {encoding:'utf8'}));
		let thisPluginForm = JSON.parse(fs.readFileSync(backendDir+"/plugins/"+a+"/settings-form.json", {encoding:'utf8'}));
		pluginPacks[a] = {
			"settings":thisPlugin,
			"settings-form":thisPluginForm,
			"path":thisPluginPath
		};
	}
	console.log("PLUGINS", pluginPacks);
	res.send(JSON.stringify(pluginPacks));
});


router.get("/get_plugin/*", async(req,res) => {
	
	
	let plugin = {};
	let a = req.params['0'];
	let thisPlugin = fs.readFileSync(backendDir+"/plugins/"+a+"/settings.json", {encoding:'utf8'});
	let thisPluginForm = fs.readFileSync(backendDir+"/plugins/"+a+"/settings-form.json", {encoding:'utf8'});
	let thisPluginIcon = backendDir+"/overlay/"+a+"/icon.png";
	plugin[a] = {
		"settings":thisPlugin,
		"settings-form":thisPluginForm,
		"icon":thisPluginIcon
	}
	
	
	res.send(JSON.stringify(plugin));
});

router.get("/get_eventsub", async(req,res) => {
	await getAppToken();

	await Axios({
		url: 'https://api.twitch.tv/helix/eventsub/subscriptions',
		method: 'GET',
		headers:{
			"Client-Id": clientId,
			"Authorization": " Bearer "+appToken,
			"Content-Type": "application/json"
		}
	})
	.then((response)=>{
		
		res.send(JSON.stringify(response.data));
	}).catch(error=>{
		console.error(error);
		return;
	});
});

router.get("/delete_eventsub", async(req,res) => {
	
	await Axios({
		url: 'https://api.twitch.tv/helix/eventsub/subscriptions?id='+req.query.id,
		method: 'DELETE',
		headers:{
			"Client-Id": clientId,
			"Authorization": " Bearer "+appToken,
			"Content-Type": "application/json"
		}
	})
	.then((response)=>{
		
		res.send(JSON.stringify({status:"SUCCESS"}));
	}).catch(error=>{
		console.error(error);
		return;
	});
});

router.get("/init_followsub", async(req,res) => {
	let subStatus = await initEventSub(req.query.type);
	console.log(subStatus);
	res.send(JSON.stringify({status:subStatus}));
});

//HTTPS ROUTER
router.post("/webhooks/callback", async (req, res) => {
	const messageType = req.header("Twitch-Eventsub-Message-Type");
	if (messageType === "webhook_callback_verification") {
		console.log("Verifying Webhook");
		return res.status(200).send(req.body.challenge);
	}

	const { type } = req.body.subscription;
	const { event } = req.body;

	console.log(
		`Receiving ${type} request for ${event.broadcaster_user_name}: `,
		event
	);
	if(config.eventsub.enabled){
		sendToTCP("/eventsub/"+type, JSON.stringify(event));
	}

  res.status(200).end();
});

app.listen(expressPort);

console.log("Spooder Web UI is running at", "http://"+config.network.host+":"+expressPort);

var udpConfig = {
	type:'udp4',
	open: {
		host: config.network.host,
		port: config.network.osc_udp_port,
		exclusive: false
	},
	send:{
		port: config.network.osc_udp_port
	}
};

osc = new OSC({plugin: new OSC.DatagramPlugin(udpConfig)});

osc.on("*", message =>{
		console.log("I heard something! ", message);
	});
	osc.on("open", () =>{
		
	});
	osc.open();

oscTCP = new OSC({plugin: new OSC.WebsocketServerPlugin({host:"0.0.0.0",port:config.network.osc_tcp_port})});

oscTCP.on("open", () =>{
	console.log("OSC TCP OPEN");
	
});

oscTCP.on("*", message => {
	let oscvars = Object.assign(config["oscvars"]);
	if(message.address.endsWith("/connect")){
		oscTCP.send(new OSC.Message(message.address.split("/")[1]+'/connect/success', 1.0));
		return;
	}
	if(message.address == '/yougood'){
		oscTCP.send(new OSC.Message('/imgood', 1.0));
		return;
	}
	
	if(message.address.startsWith("/settings")){
		let addressSplit = message.address.split("/");
		let pluginName = addressSplit[addressSplit.length-1];
		let settingsJSON = fs.readFileSync(backendDir+"/plugins/"+pluginName+"/settings.json",{encoding:'utf8'});
		oscTCP.send(new OSC.Message("/"+pluginName+"/settings", settingsJSON));
		return;
	}
	
	console.log("I heard something! (TCP)", message);
});

updateOSCListeners();
oscTCP.open();

function restartOSC(){
	
	udpConfig = {
		type:'udp4',
		open: {
			host: config.network.host,
			port: config.network.osc_udp_port,
			exclusive: false
		},
		send:{
			port: config.network.osc_udp_port
		}
	};
	
	osc = new OSC({plugin: new OSC.DatagramPlugin(udpConfig)});

	osc.on("*", message =>{
			console.log("I heard something! ", message);
		});
		osc.on("open", () =>{
			
		});
		osc.open();

	oscTCP = new OSC({plugin: new OSC.WebsocketServerPlugin({host:"0.0.0.0",port:config.network.osc_tcp_port})});

	oscTCP.on("open", () =>{
		console.log("OSC OPEN");
		
	});

	oscTCP.on("*", message =>{
		let oscvars = Object.assign(config["oscvars"]);
		if(message.address.endsWith("/connect")){
			oscTCP.send(new OSC.Message(message.address.split("/")[1]+'/connect/success', 1.0));
			return;
		}
		if(message.address == '/yougood'){
			oscTCP.send(new OSC.Message('/imgood', 1.0));
			return;
		}
		
		if(message.address.startsWith("/settings")){
			let addressSplit = message.address.split("/");
			let pluginName = addressSplit[addressSplit.length-1];
			let settingsJSON = fs.readFileSync(backendDir+"/plugins/"+pluginName+"/settings.json",{encoding:'utf8'});
			oscTCP.send(new OSC.Message("/"+pluginName+"/settings", settingsJSON));
			return;
		}
		
		console.log("I heard something! (TCP)", message);
	});

	updateOSCListeners();
	oscTCP.open();
}

const run = async() => {

	if(config.eventsub.enabled){
		await getAppToken();
	}
	
	await getBroadcasterID();

	chat = new Chat({
		username,
		token,
		onAuthenticationFailure,
	});
	
	await chat.connect().catch(error=>{console.error(error)});
	await chat.join(channel).catch(error=>{console.error(error)});

	function sayAlreadyOn(name){
		if(activeEvents[name]["etype"] == "timed"){
			chat.say(channel, name+" is already on. Time Left: "+Math.abs(Math.floor(uptime-activeEvents[name]["timeout"]))+"s");
		}else if(activeEvents[name]["etype"] == "oneshot"){
			chat.say(channel, name+" is cooling down. Time Left: "+Math.abs(Math.floor(uptime-activeEvents[name]["timeout"]))+"s");
		}
	}
	
	chat.on("*", (message) =>{
		
		if(typeof message.message == "undefined"){return;}
		
		if(message.message.startsWith("!stop")){
			let cEvent = message.message.split(" ")[1];
			if(typeof activeEvents[cEvent] != "undefined"){
				activeEvents[cEvent]["function"]();
				delete activeEvents[cEvent];
			}
			chat.say(channel, cEvent+" has been stopped!");
		}
		
		if(message.message.startsWith("!")){
			let command = message.message.substr(1).split(" ");
			for(let e in events){
				if(command[0] == e){
					
					switch(events[e].type){
						case "response":
							
							let vars = {
								sender:message.username,
								senderTags:message.tags
							};
							
							let responseFunct = eval("() => { let vars = "+JSON.stringify(vars)+"; "+events[e].message.replace(/\n/g, "")+"}");
							
							let response = responseFunct();
							chat.say(channel, response);
							break;
						case "event":
							if(events[e].etype == "timed"){
								let eventDuration = parseInt(events[e].duration);
								if(command.length>1){
									eventDuration = isNaN(command[command.length-1]) ? parseInt(events[e].duration):parseInt(command[command.length-1]);
								}
								
								if(typeof activeEvents[e] == "undefined"){
									chat.say(channel, message.username+" has activated "+events[e].name+" for "+convertDuration(eventDuration)+"!");
									sendToTCP("/events/start/"+e, message.username+" has activated "+events[e].name+" for "+convertDuration(eventDuration)+"!");
									
									sendToUDP(events[e].dest_udp, events[e].address, events[e].valueOn);
									createTimeout(e, events[e].etype, function(){
										sendToUDP(events[e].dest_udp, events[e].address, events[e].valueOff);
										chat.say(channel, events[e].name+" is now deactivated!");
									}, eventDuration);
								}else{
									sayAlreadyOn(e);
								}
							}else if(events[e].etype == "oneshot"){
								let eventCooldown = parseInt(events[e].duration);
								if(typeof activeEvents[e] == "undefined"){
									sendToUDP(events[e].dest_udp, events[e].address, events[e].valueOn);
									setTimeout(function(){
										sendToUDP(events[e].dest_udp, events[e].address, events[e].valueOff);
									}, 500);
									createTimeout(e, events[e].etype, function(){
										console.log(events[e].name+" is ready!");
									}, eventCooldown);
									chat.say(channel, events[e].name+" triggered!");
								}else{
									sayAlreadyOn(e);
								}
							}
							break;
						default:
							chat.say(channel, "Uhh, I'm not sure what to do with this type (Oo_oO;) : "+events[e].type); 
					}
				}
			}
			
			if(command[0] == config.bot.help_command){
				if(command.length>1){
					let commands = [];
					let done = false;
					for(let e in events){
						if(events[e].type == command[1] && command.length==2){
							commands.push("!"+e);
						}else if(events[e].type == command[1] && command.length > 2){
							if(e.toUpperCase() == command[2].toUpperCase()){
								chat.say(channel, events[e].description);
								done = true;
							}
						}
					}
					for(let p in activePlugins){
						if(p.toUpperCase() == command[1].toUpperCase() && command.length==2){
							commands = Object.keys(activePlugins[p].commandList);
						}else if(command.length > 2){
							if(activePlugins[p].commandList[command[2]] != null){
								chat.say(channel, activePlugins[p].commandList[command[2]]);
								done = true;
							}
						}
						
					}
					if(commands.length == 0 && done == false){
						chat.say(channel, "I'm not sure what "+command[1]+" is (^_^;)");
					}else if(done == false){
						chat.say(channel, "Commands for "+command[1]+" are: "+stringifyArray(commands));
					}
					
				}else{
					
					chat.say(channel, "Hi, I'm "+config.bot.bot_name+". "+config.bot.introduction+" \
									Pass a command type like '!"+config.bot.help_command+" type' to show the commands for that type. \
									You can also pass the command like '!"+config.bot.help_command+" type command' to get a description of what that command does. \
									Types are: ["+stringifyArray(eventTypes)+"] Active plugins are: ["+stringifyArray(Object.keys(activePlugins))+"]");
				}
			}
		}
		
		for(p in activePlugins){
			activePlugins[p].onChat(message);
		}

	});
	upInterval = setInterval(runInterval, 1000);
};



onAuthenticationFailure = async() =>{
	var refreshParams = "?client_id="+clientId+
		"&client_secret="+clientSecret+
		"&grant_type=refresh_token"+
		"&refresh_token="+refreshToken;
		
		
	await Axios.post('https://id.twitch.tv/oauth2/token'+refreshParams)
			.then((response)=>{
				
				if(typeof response.data.access_token != "undefined"){
					token = response.data.access_token;
				}
			}).catch(error=>{
				console.error(error);
				return;
			});
	
};

getBroadcasterID = async() => {
	if(broadcasterUserID==0){
		await Axios({
			url: 'https://api.twitch.tv/helix/users?login='+config.broadcaster.username,
			method: 'get',
			headers:{
				"Authorization": "Bearer "+token,
				"Client-Id":clientId
			}
		})
		.then((response)=>{
			broadcasterUserID = response.data.data[0].id;
		}).catch(error=>{
			console.error(error);
			return;
		});
	}
}

getAppToken = async() => {
	if(appToken == ""){
		var appParams = "?client_id="+clientId+
			"&client_secret="+clientSecret+
			"&grant_type=client_credentials"+
			"&scope=user:read:follows";
		
		await Axios.post('https://id.twitch.tv/oauth2/token'+appParams)
				.then((response)=>{
					console.log(response);
					if(typeof response.data.access_token != "undefined"){
						appToken = response.data.access_token;
					}
				}).catch(error=>{
					console.error(error);
					return;
				});
	}
}

initEventSub = async(eventType) => {
	await getAppToken();
	await getBroadcasterID();
	return new Promise((res, rej)=>{
		Axios({
			url: 'https://api.twitch.tv/helix/eventsub/subscriptions',
			method: 'post',
			headers:{
				"Client-ID":clientId,
				"Authorization":"Bearer "+appToken,
				"Content-Type":"application/json"
			},
			data:{
				"type":eventType,
				"version": "1",
				"condition":{
					"broadcaster_user_id":broadcasterUserID
				},
				"transport":{
					"method": "webhook",
					"callback":config.eventsub.callback_url+"/webhooks/callback",
					"secret":"imasecretboi"
				}
			}
		}).then(response => res("SUCCESS"))
		.catch(error=>{
			console.error(error);
			res(error.response.data.message);
		});
	})
	
};

function stringifyArray(a){
	let s = "";
	for(let b in a){
		s += " "+a[b];
	}
	return s;
}

global.sendToTCP = (address, oscValue) => {
	console.log("Sending ",address, oscValue, "to", "overlay");
	let newMessage = new OSC.Message(address, oscValue)
	oscTCP.send(new OSC.Message("/frontend/monitor",JSON.stringify({"types":newMessage.types,"address":address, "content":oscValue})));
	oscTCP.send(newMessage);
}

global.sendToUDP = (dest, address, oscValue) => {
	console.log("SENDING TO UDP", dest, address, oscValue);
	let valueType = "int";
	if(!isNaN(oscValue)){valueType = "i"}
	else if(!isNaN(oscValue.split(",")[0])){valueType = "ii"}
	else{valueType = "s"}
	
	if(dest == -1){return;}
	else if(dest == -2){
		let allMessage = new OSC.Message(address, oscValue);
		if(valueType == "ii"){
			oscValue = oscValue.split(",");
			allMessage = new OSC.Message(address, parseInt(oscValue[0]), parseFloat(oscValue[1]));
		}
		for(let u in udpClients){
			osc.send(allMessage, {host: udpClients[u].ip, port: udpClients[u].port});
		}
	}else{
		let message = new OSC.Message(address, oscValue);
		if(valueType == "ii"){
			oscValue = oscValue.split(",");
			message = new OSC.Message(address, parseInt(oscValue[0]), parseFloat(oscValue[1]));
		}
		osc.send(message, {host:udpClients[dest].ip, port:udpClients[dest].port});
	}
}

function updateOSCListeners(){
	
	for(let o in config.oscvars){
		
		if(o=="sectionname"){continue;}
		if(config.oscvars[o]["handlerFrom"] == "tcp"){
			oscTCP.on(config.oscvars[o]["addressFrom"], message => {
				switch(config.oscvars[o]["handlerTo"]){
					case "tcp":
						sendToTCP(config.oscvars[o]["addressTo"], message.args[0]);
					break;
					case "udp":
						sendToUDP(-2,config.oscvars[o]["addressTo"], message.args.join(","));
					break;
					default:
						sendToUDP(config.oscvars[o]["handlerTo"], config.oscvars[o]["addressTo"], message.args.join(","));
				}
			});
		}else{
			
			osc.on(config.oscvars[o]["addressFrom"], message => {
				
				switch(config.oscvars[o]["handlerTo"]){
					case "tcp":
						sendToTCP(config.oscvars[o]["addressTo"], message.args[0]);
					break;
					case "udp":
						sendToUDP(-2,config.oscvars[o]["addressTo"], message.args.join(","));
					break;
					default:
						sendToUDP(config.oscvars[o]["handlerTo"], config.oscvars[o]["addressTo"], message.args.join(","));
				}
			});
		}
	}
}

function convertDuration(numSeconds){
	let timeTerm = "seconds";
	let timeAmount = numSeconds;
	
	if(numSeconds/60 == 1){
		timeTerm = "minute";
		timeAmount = numSeconds/60;
	}else if(numSeconds/60 > 1){
		timeTerm = "minutes";
		timeAmount = numSeconds/60;
	}
	return timeAmount+" "+timeTerm;
}

runInterval = () => {
	uptime = Math.floor(Date.now()/1000);
	for(let e in activeEvents){
		console.log(e, uptime-activeEvents[e]["timeout"], uptime, activeEvents[e]["timeout"]);
		sendToTCP("/events/time/"+e, uptime-activeEvents[e]["timeout"]);
		if(uptime >= activeEvents[e]["timeout"]){
			console.log("CALLING THIS EVENT AND DELETING");
			activeEvents[e]["function"]();
			sendToTCP("/events/end/"+e, e+" is now deactivated!");
			delete activeEvents[e];
		}
	}
};

function createTimeout(name, etype, funct, seconds){
	console.log("New Timeout", seconds);
	activeEvents[name] = {
		"function": funct,
		"timeout": uptime+seconds,
		"etype": etype
	};
}
