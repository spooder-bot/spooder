# Spooder

Spooder is a Node.js based Twitch chat bot that emits OSC with chat commands. Make commands for all your OSC supported software like Resolume, Max, TouchDesigner, or Ableton. Build custom browser overlays driven with OSC with the included sample plugin. Make OSC tunnels to connect software and overlays together.

## Installation

Use npm to install Spooder

```npm install```

Initialize the config and oath files (Needs a client id and secret from your dev.twitch.tv console)

```npm run init```

## Usage

Start Spooder and its Web UI

```npm run start```

## Making commands

Before making commands for software, add a UDP client in the Config tab.

There are two types of commands:

response - The bot responds in chat. Write a script in JS to build the response and end with ```return <your_response_var>;```
event - The bot sends an OSC message for a set time or in one shot. The duration variable sets the off value after the given time. It is also a cooldown time for one-shots. The description field is what the bot will say in the !help command.

### !stop command

Call ```!stop <event>``` along with the event name to end it early.

### !help command

This will introduce the bot in chat and tell what all the commands do. You can also change the command in the web UI. It will tell which types of commands and plugin commands are available. You can show all the event commands by saying ```!help event``` then get a description of a command with ```!help event <command_name>```

## OSC Tunnels

In the Config tab, the OSC Vars section will listen for OSC commands and repeat them to another client and address. This way you can use OSC software to control your browser overlays and vice versa.

## Working with plugins

Plugins are OSC driven browser overlays that define their own chat commands. The plugins folder contains a plugin's commands, settings, and settings form for the web UI. The overlay folder is where your browser navigates to for the web files.

### Packaging plugins

Take both of your plugin's folders from the plugin and overlay folder and name them "command" and "overlay" respectively. Then compress them in a zip file. Then the plugin can be installed through the web UI in the Plugins tab.

## Support the Project!

Check out my Ko-Fi to get development updates and plugins I've created for Spooder
https://ko-fi.com/greyfursole